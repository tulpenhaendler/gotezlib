module gitlab.com/tulpenhaendler/gotezlib

go 1.12

require (
	github.com/GoKillers/libsodium-go v0.0.0-20171022220152-dd733721c3cb
	github.com/prettymuchbryce/hellobitcoin v0.0.0-20170322155636-a32469c515ed // indirect
	gitlab.com/tulpenhaendler/hellotezos v0.0.0-20180317210603-ff0b0b8d074b
)
