package crypto

import "fmt"

func AddPrefix(key string, p []byte) []byte {
	return append(Prefix(key),p...)
}

func Prefix(key string) []byte {
	switch key {
		case "tz1":
			return []byte{6, 161, 159}
		case "tz2":
			return []byte{6, 161, 161}
		case "tz3":
			return []byte{6, 161, 164}
		case "KT":
			return []byte{2,90,121}
		case "edpk":
			return []byte{13, 15, 37, 217}
		case "edsk2":
			return []byte{13, 15, 58, 7}
		case "spsk":
			return []byte{17, 162, 224, 201}
		case "p2sk":
			return []byte{16,81,238,189}
		case "sppk":
			return []byte{3, 254, 226, 86}
		case "p2pk":
			return []byte{3, 178, 139, 127}
		case "edesk":
			return []byte{7, 90, 60, 179, 41}
		case "edsk":
			return []byte{43, 246, 78, 7}
		case "edsig":
			return []byte{9, 245, 205, 134, 18}
		case "spsig1":
			return []byte{13, 115, 101, 19, 63}
		case "p2sig":
			return []byte{54, 240, 44, 52}
		case "sig":
			return []byte{4, 130, 43}
		case "Net":
			return []byte{87, 82, 0}
		case "nce":
			return []byte{69, 220, 169}
		case "b":
			return []byte{1,52}
		case "o":
			return []byte{5, 116}
		case "Lo":
			return []byte{133, 233}
		case "LLo":
			return []byte{29, 159, 109}
		case "P":
			return []byte{2, 170}
		case "Co":
			return []byte{79, 179}
		case "id":
			return []byte{153, 103}
			}

	fmt.Print("Key not found: ", key)
	return []byte{}
}
