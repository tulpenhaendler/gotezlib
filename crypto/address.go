package crypto

import (
	"fmt"
	"github.com/GoKillers/libsodium-go/cryptogenerichash"
	"github.com/GoKillers/libsodium-go/cryptosign"
	"gitlab.com/tulpenhaendler/hellotezos/base58check"
)
// Address[0] - pkh
// Address[1] - pk
// Address[2] - sk
// Address[3] type, tz1,tz2 etx

type Address [4][]byte


func (this *Address) InitRandom() error {
	sk, pk, _ := cryptosign.CryptoSignKeyPair()
	pkh, _ := generichash.CryptoGenericHash(20, pk, []byte{})
	this[0] = pkh
	this[1] = pk
	this[2] = sk
	this[3] = []byte("tz1")
	return nil
}

func (this Address) Pkh() []byte{
	return this[0]
}

func (this Address) Pk() []byte{
	return this[1]
}

func (this Address) SK() []byte{
	return this[2]
}

func (this Address) Type() string{
	fmt.Println(string(this[3]))
	return string(this[3])
}


func (this Address) PkString() string {
	return base58check.Encode("00", AddPrefix("edpk",this.Pk()))
}

func (this Address) SkString() string {
	return base58check.Encode("00", AddPrefix("edsk",this.Pk()))
}

func (this Address) PkhString() string {
	return base58check.Encode("00", AddPrefix(this.Type(),this.Pkh()))
}
