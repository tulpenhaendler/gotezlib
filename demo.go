package main

import (
	"fmt"
	"gitlab.com/tulpenhaendler/gotezlib/crypto"
)

func main(){
	a := crypto.Address{}
	a.InitRandom()

	fmt.Println("Random PKH:", a.PkhString())
	fmt.Println("Random PK:", a.PkString())
	fmt.Println("Random SK:", a.SkString())
}
